BASEDIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

PORT ?= 8080

all: 3rdparty config.txt

run: 3rdparty
	DOCROOT=${BASEDIR}/www/ PORT=$(PORT) \
		${BASEDIR}/3rdparty/lighttpd/sbin/lighttpd -D -f ${BASEDIR}/lighttpd.conf

config.txt: config_example.txt
	cp $< $@

.PHONY: prepare_offline
prepare_offline:
	$(MAKE) -C 3rdparty download

# ----- websrc

websrcs := \
	./www/3rdparty/bootstrap.min.css \
	./www/3rdparty/bootstrap.min.css.map \
	./www/3rdparty/bootstrap.bundle.min.js \
	./www/3rdparty/bootstrap.bundle.min.js.map \
	./www/3rdparty/tabulator-v5.0.min.js \
	./www/3rdparty/tabulator-v5.0.min.js.map \
	./www/3rdparty/tabulator-v5.0.min.css.map \
	./www/3rdparty/tabulator-v5.0.min.css \
	./www/3rdparty/jsoneditor.min.js \
	./www/3rdparty/jsoneditor.min.css \
	./www/3rdparty/img/jsoneditor-icons.svg \
	./www/3rdparty/jsoneditor.map \
	./www/3rdparty/diff.js \
	./www/3rdparty/diff2html.min.js \
	./www/3rdparty/diff2html-ui.min.js \
	./www/3rdparty/diff2html.min.css
	
./www/3rdparty/img/:
	mkdir -p ./www/3rdparty/img/

./www/3rdparty/%: 3rdparty/www/%.gz ./www/3rdparty/img/
	echo $*.gz $@
	zcat < 3rdparty/www/$*.gz > $@ 

# ----- 3rdparty

.PHONY: 3rdparty
3rdparty: $(websrcs)
	$(MAKE) -C 3rdparty

.PHONY: distclean
distclean:
	$(MAKE) -C 3rdparty clean
	$(RM) -r www/3rdparty/
