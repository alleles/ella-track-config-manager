
# ELLA track config manager

Read and write ELLA track configurations.

## Quick start

  * Clone the repository and change into directory  
     `git clone ... && cd ...`
  * Build  
     `make`
  * Edit configuration  
     Edit `config.txt`
  * Start web UI  
    `PORT=8080 make run`

### Installing on TSD
  * Outside TSD: After cloning the repo, run `make prepare_offline`
  * Copy the whole directory into TSD and proceed with the steps above
