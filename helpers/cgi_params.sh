# decode percent encoding ("%3C" -> ",", ...)
decode_url() {
  echo "$1" | sed 's@+@ @g;s@%@\\x@g' | xargs -0 printf "%b"
}
# parse parameters
IFS='=&' PARAMS=($QUERY_STRING)
# each parameter gets stored in its own variable
for ((i=0; i<${#PARAMS[@]}; i+=2))
do
  declare -r PARAM_${PARAMS[i]}="$(decode_url "${PARAMS[i+1]}")"
done
unset IFS
