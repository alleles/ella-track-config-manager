#!/usr/bin/env bash

get_path_for_instance_name() {
  local instance_name_query="$1"
  while IFS=$'\t' read instance_name path; do
    if [ "${instance_name}" == "${instance_name_query}" ]; then
      printf "%s\n" "${path}"
      return 0
    fi
  done < <(cat "${BASEDIR}"/config.txt | grep -v "^#" | grep ".")
  # nothing found
  return 1
}
