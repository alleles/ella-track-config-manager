#!/usr/bin/env bash

set -ETeuo pipefail

declare -r BASEDIR="$( cd "$(readlink -f "$( dirname "${BASH_SOURCE[0]}" )")/.." && pwd )"

printf "Content-Type: application/json\r\n"
printf "\r\n"

cat << EOI
{
  "config_file": "$( readlink -f "${BASEDIR}"/config.txt )"
}
EOI
