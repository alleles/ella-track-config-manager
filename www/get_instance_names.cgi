#!/usr/bin/env bash

set -ETeuo pipefail

declare -r BASEDIR="$( cd "$(readlink -f "$( dirname "${BASH_SOURCE[0]}" )")/.." && pwd )"

declare -a instance_names=()
while IFS=$'\t' read instance_name path; do
  instance_names+=("${instance_name}")
done < <(cat "${BASEDIR}"/config.txt | grep -v "^#" | grep ".")


printf "Content-Type: application/json\r\n"
printf "\r\n"

# array to json
printf "[\n"
for i in ${!instance_names[@]}; do
  printf "  \"%s\"" "${instance_names[i]}"
  (( $i+1 == ${#instance_names[@]} )) || printf ","
  printf "\n"
done
printf "]\n"
