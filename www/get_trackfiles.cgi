#!/usr/bin/env bash

set -ETeuo pipefail

declare -r BASEDIR="$( cd "$(readlink -f "$( dirname "${BASH_SOURCE[0]}" )")/.." && pwd )"

source "${BASEDIR}"/helpers/cgi_params.sh
source "${BASEDIR}"/helpers/get_path_for_instance_name.sh

[ "${PARAM_instance_name}" != "" ] \
  || { printf "error: param \"instance_name\" is undefined\n"; exit 0; }

instance_path="$(
  get_path_for_instance_name "${PARAM_instance_name}"
)"

[ "${instance_path}" != "" ] \
  || { printf "error: no config found for param \"${instance_name}\"\n"; exit 0; }

# todo check dir?

declare -a track_files=( $(cd "$BASEDIR" && ls ${instance_path}/tracks/)  )

printf "Content-Type: application/json\r\n"
printf "\r\n"

# array to json
printf "[\n"
for i in ${!track_files[@]}; do
  printf "  \"%s\"" "${track_files[i]}"
  (( $i+1 == ${#track_files[@]} )) || printf ","
  printf "\n"
done
printf "]\n"
