// wraps bootstrap alert boxes
class MiniAlerts {
  static ERROR   = {id: "Error",   bsClass: "alert-danger"};
  static OK      = {id: "Success", bsClass: "alert-success"};
  static WARNING = {id: "Warning", bsClass: "alert-warning"};
  static INFO    = {id: "Info",    bsClass: "alert-info"};

  static show(id, alertType, msgs) {
    let epar = document.getElementById(id);
    let en = document.createElement('div');
    epar.appendChild(en);
    en.classList.add('alert');
    en.classList.add(alertType.bsClass);
    en.classList.add('alert-dismissible');
    en.classList.add('mt-1');
    let eb = document.createElement('button');
    en.appendChild(eb);
    eb.type = "button";
    eb.classList.add("btn-close");
    eb.setAttribute("data-bs-dismiss", "alert");
    en.insertAdjacentHTML('beforeend', `<b>${alertType.id}</b>&nbsp; ` + [].concat(msgs).join("<br>"));
  }

  static clear(id) {
    document.getElementById(id).innerHTML = "";
  }
}

const clearMsgs   = ()    => MiniAlerts.clear("minialerts")
const showMsg     = (msg) => MiniAlerts.show("minialerts", MiniAlerts.INFO,    msg)
const showError   = (msg) => MiniAlerts.show("minialerts", MiniAlerts.ERROR,   msg)
const showSuccess = (msg) => MiniAlerts.show("minialerts", MiniAlerts.OK,      msg)
const showWarning = (msg) => MiniAlerts.show("minialerts", MiniAlerts.WARNING, msg)
