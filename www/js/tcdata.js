class TcData {
  instance_id = null;
  trackcfg = null;
  tracklist = null;
  trackcfg_warnings = {};

  constructor(instance_name) {
    this.instance_id = instance_name;
  }
  
  async fetchAll() {
    this.tracklist = await TcData.fechtTrackList(this.instance_id);
    this.trackcfg = await TcData.fechtTrackCfg(this.instance_id);
    this.trackcfg_warnings = TcData.computeWarnings(
      this.trackcfg,
      this.tracklist, 
    );
    // make read-only
    Object.freeze(this.instance_id);
    Object.freeze(this.tracklist);
    Object.freeze(this.trackcfg);
    Object.freeze(this.trackcfg_warnings);
  }

  getWarnings(ruleId) {
    if (!this.trackcfg_warnings.hasOwnProperty(ruleId)) {
      return []
    }
    return this.trackcfg_warnings[ruleId];
  }

  static async fechtTrackList(instance_id) {
    // track file list
    try {
      const r = await fetch(`./get_trackfiles.cgi?instance_name=${encodeURI(instance_id)}`, {
        method: "GET"
      });
      if (!r.ok) {
        throw new Error(r.statusText);
      }
      const jsn = await r.json();
      return jsn;
    } catch (e) {
      throw new Error("Unable to load track files. " + e.toString());
    };
  }

  static async fechtTrackCfg(instance_id) {
    try {
      const r = await fetch(`./get_trackconfigs.cgi?instance_name=${encodeURI(instance_id)}`, {
        method: "GET"
      });
      if (!r.ok) {
        throw new Error(r.statusText);
      }
      const jsn = await r.json();
      return jsn;
    } catch (e) {
      throw new Error("Unable to load track configs" + e.toString());
    };
  }

  static computeWarnings(trackcfg, tracklist) {
    // init
    const warnings = Object.keys(trackcfg)
      .map(k => [k, []])
      .reduce((obj, [k, v]) => {
        obj[k] = v
        return obj
      }, {})
    // compute warnigs
    Object.entries(trackcfg).forEach(([ruleId, cfg]) => {
      try {
        const re = new RegExp(ruleId);
        if (get_type_from_trackrule(ruleId) == "STATIC") {
          const appliesToAnyStaticTrack = tracklist.some((t) => re.test(`STATIC/${t}`))
          if (!appliesToAnyStaticTrack) {
            warnings[ruleId].push("the STATIC rule does not seem to match any track files");
          }
        }
      } catch (error) {
        // most likely regex did not compile
        warnings[ruleId].push(error.message);
        return;
      }
    });
    return warnings;
  }
}

function get_type_from_trackrule(id) {
  const m = id.match(/^\^[^/]*\//);
  if (m.length == 0) {
    return "undefined"
  }
  typestr = m[0]
  typestr = typestr.replace(/^\^/, "")
  typestr = typestr.replace(/\/$/, "")
  return typestr
}
