#!/usr/bin/env bash

set -ETeuo pipefail

declare -r BASEDIR="$( cd "$(readlink -f "$( dirname "${BASH_SOURCE[0]}" )")/.." && pwd )"

source "${BASEDIR}"/helpers/cgi_params.sh
source "${BASEDIR}"/helpers/get_path_for_instance_name.sh

[ "${PARAM_instance_name}" != "" ] \
  || { printf "error: param \"instance_name\" is undefined\n"; exit 1; }

instance_path="$(
  get_path_for_instance_name "${PARAM_instance_name}"
)"

[ "${instance_path}" != "" ] \
  || { printf "error: no config found for param \"${instance_name}\"\n"; exit 1; }

cd "${BASEDIR}"
cat > "${instance_path}"/track_config.json_tmp
mv "${instance_path}"/track_config.json_tmp "${instance_path}"/track_config.json

printf "Content-type: application/json\r\n"
printf "\r\n"

# send body

printf '{ "status": "ok" }\n'
